<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doc".
 *
 * @property integer $uid
 * @property integer $id
 * @property string $tkey
 * @property string $parent
 * @property string $title
 * @property string $text
 * @property string $pos
 * @property string $active
 * @property string $view_str
 * @property string $ptitle
 * @property string $pdescription
 * @property string $pkeywords
 *
 * @property CityHasDoc[] $cityHasDocs
 * @property City[] $cities
 */
class Doc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['text'], 'string'],
            [['tkey', 'parent', 'title', 'pos', 'active', 'view_str', 'ptitle', 'pdescription', 'pkeywords'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['tkey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'id' => 'ID',
            'tkey' => 'Tkey',
            'parent' => 'Parent',
            'title' => 'Title',
            'text' => 'Text',
            'pos' => 'Pos',
            'active' => 'Active',
            'view_str' => 'View Str',
            'ptitle' => 'Ptitle',
            'pdescription' => 'Pdescription',
            'pkeywords' => 'Pkeywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityHasDocs()
    {
        return $this->hasMany(CityHasDoc::className(), ['doc_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['id' => 'city_id'])->viaTable('city_has_doc', ['doc_id' => 'id']);
    }
}
