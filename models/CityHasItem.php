<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "city_has_item".
 *
 * @property integer $city_id
 * @property integer $item_id
 * @property string $price
 * @property string $currency_price
 * @property string $days
 * @property string $days_work
 * @property string $is_enabled
 * @property string $fast_delivery_is_active
 * @property string $fast_delivery_period
 * @property string $fast_delivery_periods
 * @property string $info
 * @property string $delivery_info
 * @property string $deliverytype_period
 *
 * @property City $city
 * @property Item $item
 */
class CityHasItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_has_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'item_id'], 'required'],
            [['city_id', 'item_id'], 'integer'],
            [['fast_delivery_periods', 'info', 'delivery_info'], 'string'],
            [['price', 'currency_price', 'days', 'days_work', 'is_enabled', 'fast_delivery_is_active', 'fast_delivery_period', 'deliverytype_period'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'City ID',
            'item_id' => 'Item ID',
            'price' => 'Price',
            'currency_price' => 'Currency Price',
            'days' => 'Days',
            'days_work' => 'Days Work',
            'is_enabled' => 'Is Enabled',
            'fast_delivery_is_active' => 'Fast Delivery Is Active',
            'fast_delivery_period' => 'Fast Delivery Period',
            'fast_delivery_periods' => 'Fast Delivery Periods',
            'info' => 'Info',
            'delivery_info' => 'Delivery Info',
            'deliverytype_period' => 'Deliverytype Period',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }
}
