<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "group".
 *
 * @property integer $uid
 * @property integer $id
 * @property string $name
 * @property string $pos
 * @property string $tkey
 *
 * @property CityHasGroup[] $cityHasGroups
 * @property City[] $cities
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'pos', 'tkey'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['tkey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'id' => 'ID',
            'name' => 'Name',
            'pos' => 'Pos',
            'tkey' => 'Tkey',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityHasGroups()
    {
        return $this->hasMany(CityHasGroup::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['id' => 'city_id'])->viaTable('city_has_group', ['group_id' => 'id']);
    }
}
