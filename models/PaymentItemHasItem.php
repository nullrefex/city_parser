<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_item_has_item".
 *
 * @property string $payment_item_tkey
 * @property integer $item_id
 * @property string $tooltip
 * @property string $price
 * @property string $currency_price
 *
 * @property Item $item
 * @property PaymentItem $paymentItemTkey
 */
class PaymentItemHasItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_item_has_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_item_tkey', 'item_id'], 'required'],
            [['item_id'], 'integer'],
            [['payment_item_tkey', 'tooltip', 'price', 'currency_price'], 'string', 'max' => 255],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['payment_item_tkey'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentItem::className(), 'targetAttribute' => ['payment_item_tkey' => 'tkey']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_item_tkey' => 'Payment Item Tkey',
            'item_id' => 'Item ID',
            'tooltip' => 'Tooltip',
            'price' => 'Price',
            'currency_price' => 'Currency Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemTkey()
    {
        return $this->hasOne(PaymentItem::className(), ['tkey' => 'payment_item_tkey']);
    }
}
