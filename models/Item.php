<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item".
 *
 * @property integer $uid
 * @property integer $id
 * @property string $tkey
 * @property string $brand_tkey
 * @property string $title
 * @property string $type
 * @property string $hint
 * @property string $site_name
 * @property string $is_point
 * @property string $comment
 * @property string $description
 * @property string $duration_type
 * @property string $position
 * @property string $delivery_group
 * @property string $has_postamat
 * @property string $doc-text
 *
 * @property CityHasItem[] $cityHasItems
 * @property City[] $cities
 * @property ItemHasPostamat[] $itemHasPostamats
 * @property Postamat[] $postamats
 * @property PaymentItemHasItem[] $paymentItemHasItems
 * @property PaymentItem[] $paymentItemTkeys
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['doc-text'], 'string'],
            [['tkey', 'brand_tkey', 'title', 'type', 'hint', 'site_name', 'is_point', 'comment', 'description', 'duration_type', 'position', 'delivery_group', 'has_postamat'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['tkey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'id' => 'ID',
            'tkey' => 'Tkey',
            'brand_tkey' => 'Brand Tkey',
            'title' => 'Title',
            'type' => 'Type',
            'hint' => 'Hint',
            'site_name' => 'Site Name',
            'is_point' => 'Is Point',
            'comment' => 'Comment',
            'description' => 'Description',
            'duration_type' => 'Duration Type',
            'position' => 'Position',
            'delivery_group' => 'Delivery Group',
            'has_postamat' => 'Has Postamat',
            'doc-text' => 'Doc Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityHasItems()
    {
        return $this->hasMany(CityHasItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['id' => 'city_id'])->viaTable('city_has_item', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemHasPostamats()
    {
        return $this->hasMany(ItemHasPostamat::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostamats()
    {
        return $this->hasMany(Postamat::className(), ['id' => 'postamat_id'])->viaTable('item_has_postamat', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemHasItems()
    {
        return $this->hasMany(PaymentItemHasItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemTkeys()
    {
        return $this->hasMany(PaymentItem::className(), ['tkey' => 'payment_item_tkey'])->viaTable('payment_item_has_item', ['item_id' => 'id']);
    }
}
