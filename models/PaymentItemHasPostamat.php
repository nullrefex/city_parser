<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_item_has_postamat".
 *
 * @property string $payment_item_tkey
 * @property integer $postamat_id
 * @property string $tooltip
 * @property string $price
 * @property string $currency_price
 *
 * @property PaymentItem $paymentItemTkey
 * @property Postamat $postamat
 */
class PaymentItemHasPostamat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_item_has_postamat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_item_tkey', 'postamat_id'], 'required'],
            [['postamat_id'], 'integer'],
            [['payment_item_tkey', 'tooltip', 'price', 'currency_price'], 'string', 'max' => 255],
            [['payment_item_tkey'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentItem::className(), 'targetAttribute' => ['payment_item_tkey' => 'tkey']],
            [['postamat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Postamat::className(), 'targetAttribute' => ['postamat_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_item_tkey' => 'Payment Item Tkey',
            'postamat_id' => 'Postamat ID',
            'tooltip' => 'Tooltip',
            'price' => 'Price',
            'currency_price' => 'Currency Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemTkey()
    {
        return $this->hasOne(PaymentItem::className(), ['tkey' => 'payment_item_tkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostamat()
    {
        return $this->hasOne(Postamat::className(), ['id' => 'postamat_id']);
    }
}
