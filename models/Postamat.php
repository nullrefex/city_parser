<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "postamat".
 *
 * @property integer $uid
 * @property integer $id
 * @property string $deliverytype_id
 * @property string $deliverytype_city_id
 * @property string $postamat_id
 * @property string $zip_code
 * @property string $name
 * @property string $description
 * @property string $work_time
 * @property string $address
 * @property string $phone
 * @property string $latitude
 * @property string $longitude
 * @property string $is_active
 * @property string $updated_at
 * @property string $type
 * @property string $store_id
 * @property string $brand_tkey
 * @property string $brand_name
 * @property string $deliveryTkey
 * @property string $deliverytype_period
 * @property string $days
 * @property string $price
 * @property string $currency_price
 *
 * @property ItemHasPostamat[] $itemHasPostamats
 * @property Item[] $items
 * @property PaymentItemHasPostamat[] $paymentItemHasPostamats
 * @property PaymentItem[] $paymentItemTkeys
 */
class Postamat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postamat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['deliverytype_id', 'deliverytype_city_id', 'postamat_id', 'zip_code', 'name', 'description', 'work_time', 'address', 'phone', 'latitude', 'longitude', 'is_active', 'updated_at', 'type', 'store_id', 'brand_tkey', 'brand_name', 'deliveryTkey', 'deliverytype_period', 'days', 'price', 'currency_price'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'id' => 'ID',
            'deliverytype_id' => 'Deliverytype ID',
            'deliverytype_city_id' => 'Deliverytype City ID',
            'postamat_id' => 'Postamat ID',
            'zip_code' => 'Zip Code',
            'name' => 'Name',
            'description' => 'Description',
            'work_time' => 'Work Time',
            'address' => 'Address',
            'phone' => 'Phone',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'is_active' => 'Is Active',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'store_id' => 'Store ID',
            'brand_tkey' => 'Brand Tkey',
            'brand_name' => 'Brand Name',
            'deliveryTkey' => 'Delivery Tkey',
            'deliverytype_period' => 'Deliverytype Period',
            'days' => 'Days',
            'price' => 'Price',
            'currency_price' => 'Currency Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemHasPostamats()
    {
        return $this->hasMany(ItemHasPostamat::className(), ['postamat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('item_has_postamat', ['postamat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemHasPostamats()
    {
        return $this->hasMany(PaymentItemHasPostamat::className(), ['postamat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemTkeys()
    {
        return $this->hasMany(PaymentItem::className(), ['tkey' => 'payment_item_tkey'])->viaTable('payment_item_has_postamat', ['postamat_id' => 'id']);
    }
}
