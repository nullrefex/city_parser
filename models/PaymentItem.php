<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_item".
 *
 * @property integer $uid
 * @property string $tkey
 * @property string $title
 * @property string $hint
 * @property string $description
 * @property string $icon
 * @property string $icon_alt
 * @property string $discount
 * @property string $discount_currency
 * @property string $payment_tax
 * @property string $percent
 *
 * @property PaymentItemHasItem[] $paymentItemHasItems
 * @property Item[] $items
 * @property PaymentItemHasPostamat[] $paymentItemHasPostamats
 * @property Postamat[] $postamats
 */
class PaymentItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tkey', 'title', 'hint', 'description', 'icon', 'icon_alt', 'discount', 'discount_currency', 'payment_tax', 'percent'], 'string', 'max' => 255],
            [['tkey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'tkey' => 'Tkey',
            'title' => 'Title',
            'hint' => 'Hint',
            'description' => 'Description',
            'icon' => 'Icon',
            'icon_alt' => 'Icon Alt',
            'discount' => 'Discount',
            'discount_currency' => 'Discount Currency',
            'payment_tax' => 'Payment Tax',
            'percent' => 'Percent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemHasItems()
    {
        return $this->hasMany(PaymentItemHasItem::className(), ['payment_item_tkey' => 'tkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('payment_item_has_item', ['payment_item_tkey' => 'tkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentItemHasPostamats()
    {
        return $this->hasMany(PaymentItemHasPostamat::className(), ['payment_item_tkey' => 'tkey']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostamats()
    {
        return $this->hasMany(Postamat::className(), ['id' => 'postamat_id'])->viaTable('payment_item_has_postamat', ['payment_item_tkey' => 'tkey']);
    }
}
