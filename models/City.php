<?php

namespace app\models;

use yii\data\ArrayDataProvider;

/**
 * This is the model class for table "city".
 *
 * @property integer $uid
 * @property integer $id
 * @property string $name
 * @property string $tkey
 * @property string $region_id
 * @property string $region
 * @property string $json
 *
 * @property CityHasDoc[] $cityHasDocs
 * @property Doc[] $docs
 * @property CityHasGroup[] $cityHasGroups
 * @property Group[] $groups
 * @property CityHasItem[] $cityHasItems
 * @property Item[] $items
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['json'], 'string'],
            [['name', 'tkey', 'region_id', 'region'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['tkey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'id' => 'ID',
            'name' => 'Name',
            'tkey' => 'Tkey',
            'region_id' => 'Region ID',
            'region' => 'Region',
            'json' => 'Json',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityHasDocs()
    {
        return $this->hasMany(CityHasDoc::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocs()
    {
        return $this->hasMany(Doc::className(), ['id' => 'doc_id'])->viaTable('city_has_doc', ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityHasGroups()
    {
        return $this->hasMany(CityHasGroup::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['id' => 'group_id'])->viaTable('city_has_group', ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityHasItems()
    {
        return $this->hasMany(CityHasItem::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('city_has_item', ['city_id' => 'id']);
    }

    public function getItemsDataProvider()
    {
        $result = [];

        /** @var CityHasItem[] $items */
        $items = CityHasItem::find()->joinWith('item')->andWhere(['city_id' => $this->id])->all();

        /** @var CityHasGroup[] $groups */
        $groups = CityHasGroup::find()->joinWith('group')->andWhere(['city_id' => $this->id])->all();


        $city = $this;

        foreach ($groups as $group) {
            $result = array_reduce(array_map(function (CityHasItem $item) use ($city) {
                $orderSyntax = [
                    'BOXBERRY' => 'Пункты выдачи Boxberry',
                    'CDEK' => 'Пункты выдачи СДЭК',
                    'DPD' => 'Пункты выдачи DPD',
                    'VSEMAYKI' => 'Принт-центры Vsemayki.ru',
                ];
                /** @var ItemHasPostamat[] $postamats */
                $postamats = $item->item->getItemHasPostamats()->joinWith('postamat')->orderBy(['postamat.brand_name' => SORT_DESC])->andWhere(['city_id' => $city->id])->all();
                $result = [];
                if (count($postamats)) {
                    foreach ($postamats as $postamat) {
                        $result[$postamat->postamat->brand_name] = [
                            'tkey' => $postamat->postamat->deliveryTkey,
                            'title' => $orderSyntax[$postamat->postamat->brand_tkey],
                            'days' => $postamat->postamat->days,
                            'price' => $postamat->postamat->price . " руб.",
                            'postamats' => [],
                        ];
                    }
                    foreach ($postamats as $postamat) {
                        $result[$postamat->postamat->brand_name]['postamats'][] = $postamat;
                    }
                    return array_values($result);
                }
                return [[
                    'tkey' => $item->item->tkey,
                    'title' => $item->item->site_name,
                    'days' => $item->days,
                    'price' => $item->price . " руб.",
                ]];
            }, array_filter($items, function (CityHasItem $item) use ($group) {
                return $item->item->delivery_group === $group->group->tkey;
            })), function ($carry, $item) {
                return array_merge($carry, $item);
            }, $result);
        }

        return new ArrayDataProvider(['models' => $result]);
    }
}
