<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_has_postamat".
 *
 * @property integer $item_id
 * @property integer $postamat_id
 * @property integer $city_id
 *
 * @property City $city
 * @property Item $item
 * @property Postamat $postamat
 */
class ItemHasPostamat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_has_postamat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'postamat_id', 'city_id'], 'required'],
            [['item_id', 'postamat_id', 'city_id'], 'integer'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['postamat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Postamat::className(), 'targetAttribute' => ['postamat_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'postamat_id' => 'Postamat ID',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostamat()
    {
        return $this->hasOne(Postamat::className(), ['id' => 'postamat_id']);
    }
}
