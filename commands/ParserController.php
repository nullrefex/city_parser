<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\commands;


use app\components\Loader;
use app\models\City;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Execute parse process
 *
 * @package app\commands
 */
class ParserController extends Controller
{
    /**
     * Run sync parsing
     */
    public function actionParse()
    {
        Console::output('Started ' . \Yii::$app->formatter->asDatetime(time()));
        $loader = new Loader();

        $list = $this->getList();

        $loader->load($list);

        Console::output('Parsed ' . \Yii::$app->formatter->asDatetime(time()));
    }

    /**
     * Run concurrent parsing
     */
    public function actionParseAsync()
    {
        Console::output('Started ' . \Yii::$app->formatter->asDatetime(time()));
        $loader = new Loader();

        $list = $this->getList();

        $loader->loadAsync($list, 20);

        Console::output('Parsed ' . \Yii::$app->formatter->asDatetime(time()));
    }

    /**
     * @param $name
     */
    public function actionParseSingle($name)
    {
        Console::output('Started ' . \Yii::$app->formatter->asDatetime(time()));
        $loader = new Loader();

        $loader->loadCity($name);

        Console::output('Parsed ' . \Yii::$app->formatter->asDatetime(time()));
    }

    /**
     * @return array
     */
    protected function getList()
    {
        $cities = City::find()
            ->asArray()
            ->all();

        return array_map(function ($item) {
            return [
                'city' => $item['name'],
                'area' => $item['region'],
            ];
        }, $cities);
    }
}