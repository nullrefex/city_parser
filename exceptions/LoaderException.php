<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\exceptions;


use yii\base\Exception;

class LoaderException extends Exception
{

}