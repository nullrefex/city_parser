<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\controllers;


use VseMayki\RestConnector;
use yii\web\Controller;

class ApiController extends Controller
{
    /** @var RestConnector */
    protected $client;

    public function init()
    {
        $clientId = 'shop_89575';
        $clientSecret = 'deb618bbe5d394c304416535c387a8ed';

        $this->client = new RestConnector($clientId, $clientSecret);

        parent::init();
    }

    public function actionIndex()
    {
        echo '<pre>';
        $result = $this->client->sendRequest('/catalog/products', []);
        print_r($result);
    }

    public function actionProduct($id = '140029_manshort')
    {
        echo '<pre>';
        $result = $this->client->sendRequest('/catalog/item/' . $id, []);
        print_r($result);
    }
}