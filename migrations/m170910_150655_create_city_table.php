<?php

use yii\db\Migration;

class m170910_150655_create_city_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%city}}', [
            'uid' => $this->primaryKey(),
            'id' => $this->integer()->unique(),
            'name' => $this->string(),
            'tkey' => $this->string()->unique(),
            'region_id' => $this->string(),
            'region' => $this->string(),
            'json' => $this->text(),
        ]);

        $this->createIndex(
            'idx-city-id',
            '{{%city}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropIndex('idx-city-id', '{{%city}}');
        $this->dropTable('{{%city}}');
    }
}
