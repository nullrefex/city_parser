<?php

use yii\db\Migration;

class m170910_150656_create_item_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%item}}', [
            'uid' => $this->primaryKey(),
            'id' => $this->integer()->unique(),
            'tkey' => $this->string()->unique(),
            'brand_tkey' => $this->string(),
            'title' => $this->string(),
            'type' => $this->string(),
            'hint' => $this->string(),
            'site_name' => $this->string(),
            'is_point' => $this->string(),
            'comment' => $this->string(),
            'description' => $this->string(),
            'duration_type' => $this->string(),
            'position' => $this->string(),
            'delivery_group' => $this->string(),
            'has_postamat' => $this->string(),
            'doc-text' => $this->text(),
        ]);

        $this->createIndex(
            'idx-item-id',
            '{{%item}}',
            'id'
        );

        $this->createTable('{{%city_has_item}}', [
            'city_id' => $this->integer(),
            'item_id' => $this->integer(),
            'price' => $this->string(),
            'currency_price' => $this->string(),
            'days' => $this->string(),
            'days_work' => $this->string(),
            'is_enabled' => $this->string(),
            'fast_delivery_is_active' => $this->string(),
            'fast_delivery_period' => $this->string(),
            'fast_delivery_periods' => $this->text(),
            'info' => $this->text(),
            'delivery_info' => $this->text(),
            'deliverytype_period' => $this->string(),
        ]);

        $this->addPrimaryKey('city_has_item', '{{%city_has_item}}', [
            'city_id',
            'item_id',
        ]);

        $this->addForeignKey(
            'fk-city_has_item-city_id',
            '{{%city_has_item}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-city_has_item-item_id',
            '{{%city_has_item}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-city_has_item-item_id', '{{%city_has_item}}');
        $this->dropForeignKey('fk-city_has_item-city_id', '{{%city_has_item}}');
        $this->dropTable('{{%city_has_item}}');

        $this->dropIndex('idx-item-id', '{{%item}}');
        $this->dropTable('{{%item}}');
    }
}
