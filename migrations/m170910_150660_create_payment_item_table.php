<?php

use yii\db\Migration;

class m170910_150660_create_payment_item_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%payment_item}}', [
            'uid' => $this->primaryKey(),
            'tkey' => $this->string()->unique(),
            'title' => $this->string(),
            'hint' => $this->string(),
            'description' => $this->string(),
            'icon' => $this->string(),
            'icon_alt' => $this->string(),
            'discount' => $this->string(),
            'discount_currency' => $this->string(),
            'payment_tax' => $this->string(),
            'percent' => $this->string(),
        ]);

        $this->createIndex(
            'idx-payment_item-tkey',
            '{{%payment_item}}',
            'tkey'
        );

        $this->createTable('{{%payment_item_has_postamat}}', [
            'payment_item_tkey' => $this->string(),
            'postamat_id' => $this->integer(),
            'tooltip' => $this->string(),
            'price' => $this->string(),
            'currency_price' => $this->string(),
        ]);

        $this->addPrimaryKey('payment_item_has_postamat', '{{%payment_item_has_postamat}}', [
            'payment_item_tkey',
            'postamat_id',
        ]);

        $this->addForeignKey(
            'fk-payment_item_has_postamat-payment_item_tkey',
            '{{%payment_item_has_postamat}}',
            'payment_item_tkey',
            '{{%payment_item}}',
            'tkey',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-payment_item_has_postamat-postamat_id',
            '{{%payment_item_has_postamat}}',
            'postamat_id',
            '{{%postamat}}',
            'id',
            'CASCADE'
        );


        $this->createTable('{{%payment_item_has_item}}', [
            'payment_item_tkey' => $this->string(),
            'item_id' => $this->integer(),
            'tooltip' => $this->string(),
            'price' => $this->string(),
            'currency_price' => $this->string(),
        ]);

        $this->addPrimaryKey('payment_item_has_item', '{{%payment_item_has_item}}', [
            'payment_item_tkey',
            'item_id',
        ]);

        $this->addForeignKey(
            'fk-payment_item_has_item-payment_item_tkey',
            '{{%payment_item_has_item}}',
            'payment_item_tkey',
            '{{%payment_item}}',
            'tkey',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-payment_item_has_item-item_id',
            '{{%payment_item_has_item}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-payment_item_has_postamat-postamat_id', '{{%payment_item_has_postamat}}');
        $this->dropForeignKey('fk-payment_item_has_postamat-payment_item_tkey', '{{%payment_item_has_postamat}}');
        $this->dropTable('{{%payment_item_has_postamat}}');

        $this->dropForeignKey('fk-payment_item_has_item-payment_item_tkey', '{{%payment_item_has_item}}');
        $this->dropForeignKey('fk-payment_item_has_item-item_id', '{{%payment_item_has_item}}');
        $this->dropTable('{{%payment_item_has_item}}');

        $this->dropIndex('idx-payment_item-tkey', '{{%payment_item}}');
        $this->dropTable('{{%payment_item}}');
    }
}
