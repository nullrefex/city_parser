<?php

use yii\db\Migration;

class m170910_150659_create_postamat_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%postamat}}', [
            'uid' => $this->primaryKey(),
            'id' => $this->integer()->unique(),
            'deliverytype_id' => $this->string(),
            'deliverytype_city_id' => $this->string(),
            'postamat_id' => $this->string(),
            'zip_code' => $this->string(),
            'name' => $this->string(),
            'description' => $this->string(),
            'work_time' => $this->string(),
            'address' => $this->string(),
            'phone' => $this->string(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'is_active' => $this->string(),
            'updated_at' => $this->string(),
            'type' => $this->string(),
            'store_id' => $this->string(),
            'brand_tkey' => $this->string(),
            'brand_name' => $this->string(),
            'deliveryTkey' => $this->string(),
            'deliverytype_period' => $this->string(),
            'days' => $this->string(),
            'price' => $this->string(),
            'currency_price' => $this->string(),
        ]);

        $this->createIndex(
            'idx-postamat-id',
            '{{%postamat}}',
            'id'
        );

        $this->createTable('{{%item_has_postamat}}', [
            'item_id' => $this->integer(),
            'postamat_id' => $this->integer(),
            'city_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('item_has_postamat', '{{%item_has_postamat}}', [
            'item_id',
            'postamat_id',
            'city_id',
        ]);

        $this->addForeignKey(
            'fk-item_has_postamat-item_id',
            '{{%item_has_postamat}}',
            'item_id',
            '{{%item}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-item_has_postamat-postamat_id',
            '{{%item_has_postamat}}',
            'postamat_id',
            '{{%postamat}}',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-item_has_postamat-city_id',
            '{{%item_has_postamat}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-item_has_postamat-city_id', '{{%item_has_postamat}}');
        $this->dropForeignKey('fk-item_has_postamat-item_id', '{{%item_has_postamat}}');
        $this->dropForeignKey('fk-item_has_postamat-postamat_id', '{{%item_has_postamat}}');
        $this->dropTable('{{%item_has_postamat}}');
        
        $this->dropIndex('idx-postamat-id', '{{%postamat}}');
        $this->dropTable('{{%postamat}}');
    }
}
