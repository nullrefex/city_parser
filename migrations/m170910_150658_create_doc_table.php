<?php

use yii\db\Migration;

class m170910_150658_create_doc_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%doc}}', [
            'uid' => $this->primaryKey(),
            'id' => $this->integer()->unique(),
            'tkey' => $this->string()->unique(),
            'parent' => $this->string(),
            'title' => $this->string(),
            'text' => $this->text(),
            'pos' => $this->string(),
            'active' => $this->string(),
            'view_str' => $this->string(),
            'ptitle' => $this->string(),
            'pdescription' => $this->string(),
            'pkeywords' => $this->string(),
        ]);

        $this->createIndex(
            'idx-doc-id',
            '{{%doc}}',
            'id'
        );

        $this->createTable('{{%city_has_doc}}', [
            'city_id' => $this->integer(),
            'doc_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('city_has_doc', '{{%city_has_doc}}', [
            'city_id',
            'doc_id',
        ]);

        $this->addForeignKey(
            'fk-city_has_doc-city_id',
            '{{%city_has_doc}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-city_has_doc-doc_id',
            '{{%city_has_doc}}',
            'doc_id',
            '{{%doc}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-city_has_doc-doc_id', '{{%city_has_doc}}');
        $this->dropForeignKey('fk-city_has_doc-city_id', '{{%city_has_doc}}');
        $this->dropTable('{{%city_has_doc}}');

        $this->dropIndex('idx-doc-id', '{{%doc}}');
        $this->dropTable('{{%doc}}');
    }
}
