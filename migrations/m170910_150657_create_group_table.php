<?php

use yii\db\Migration;

class m170910_150657_create_group_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%group}}', [
            'uid' => $this->primaryKey(),
            'id' => $this->integer()->unique(),
            'name' => $this->string(),
            'pos' => $this->string(),
            'tkey' => $this->string()->unique(),
        ]);

        $this->createIndex(
            'idx-group-id',
            '{{%group}}',
            'id'
        );

        $this->createTable('{{%city_has_group}}', [
            'city_id' => $this->integer(),
            'group_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('city_has_group', '{{%city_has_group}}', [
            'city_id',
            'group_id',
        ]);

        $this->addForeignKey(
            'fk-city_has_group-city_id',
            '{{%city_has_group}}',
            'city_id',
            '{{%city}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-city_has_group-group_id',
            '{{%city_has_group}}',
            'group_id',
            '{{%group}}',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-city_has_group-group_id', '{{%city_has_group}}');
        $this->dropForeignKey('fk-city_has_group-city_id', '{{%city_has_group}}');
        $this->dropTable('{{%city_has_group}}');

        $this->dropIndex('idx-group-id', '{{%group}}');
        $this->dropTable('{{%group}}');
    }
}
