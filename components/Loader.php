<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\components;


use app\exceptions\LoaderException;
use app\models\City;
use app\models\CityHasDoc;
use app\models\CityHasGroup;
use app\models\CityHasItem;
use app\models\Doc;
use app\models\Group;
use app\models\Item;
use app\models\ItemHasPostamat;
use app\models\PaymentItem;
use app\models\PaymentItemHasItem;
use app\models\PaymentItemHasPostamat;
use app\models\Postamat;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Yii;
use yii\base\Component;
use yii\helpers\Console;
use yii\helpers\Inflector;
use yii\helpers\Json;

class Loader extends Component
{
    public $headers = [
        'content-type' => 'application/x-www-form-urlencoded; charset=UTF-8',
        'accept-encoding' => 'gzip, deflate, br',
        'accept' => '*/*',
        'x-requested-with' => 'XMLHttpRequest',
    ];

    public $uri = 'https://www.vsemayki.ru/cart/deliveryList';

    public $filePath = '@app/cities_and_regions_rf.txt';

    public $errorFilePath = '@app/errors.txt';

    public $validateModels = false;

    /**
     *
     */
    public function init()
    {
        ini_set('memory_limit', -1);
        set_time_limit(-1);
        parent::init();
    }

    /**
     * @param $list
     */
    public function load($list)
    {
        $client = new Client();

        $this->log('Get list of ' . count($list) . ' items');

        foreach ($list as $index => $item) {
            $response = $this->request($client, $item);
            $this->processResponse($response, $item, $index);
        }
    }

    /**
     * @param $city
     * @param string $area
     */
    public function loadCity($city, $area = '')
    {
        $client = new Client();

        $item = [
            'city' => $city,
            'area' => $area,
        ];

        $this->log('Start loading ' . $item['city']);
        $response = $this->request($client, $item);
        $this->processResponse($response, $item, 0);
    }

    /**
     * @param $list
     * @param int $concurrency
     */
    public function loadAsync($list, $concurrency = 5)
    {
        return;
/*        $client = new Client();
        $loader = $this;

        $requests = function ($list) use ($client, $loader) {
            foreach ($list as $item) {
                yield function () use ($client, $loader, $item) {
                    return $loader->getAsyncRequest($client, $item);
                };
            }
        };

        $pool = new Pool($client, $requests($list), [
            'concurrency' => $concurrency,
            'fulfilled' => function ($response, $index) use ($loader, $list) {
                $loader->processResponse($response, $list[$index], $index);
            },
            'rejected' => function ($reason, $index) use ($loader, $list) {
                $loader->log('Can\'t load data for ' . $list[$index]['city']);
                $loader->log(print_r($reason, true));
            },
        ]);

        $promise = $pool->promise();

        $promise->wait();*/
    }

    /**
     * @param $string
     */
    public function log($string)
    {
        Console::output($string);
    }

    /**
     * @param Client $client
     * @param $item
     * @return mixed
     */
    protected function request($client, $item)
    {
        $response = $client->request('POST', $this->uri, $this->createRequestOptions($item));
        return $response;
    }

    /**
     * @param Client $client
     * @param $item
     * @return \GuzzleHttp\Promise\PromiseInterface
     */
    public function getAsyncRequest($client, $item)
    {
        return $client->postAsync($this->uri, $this->createRequestOptions($item));
    }

    /**
     * @param $data
     * @return bool
     */
    protected function validateData($data)
    {
        if (!is_array($data)) {
            return false;
        }
        if (!isset($data['city'])) {
            return false;
        }
        if (!isset($data['items'])) {
            return false;
        }
        if (!is_array($data['items'])) {
            return false;
        }
        if (!isset($data['groups'])) {
            return false;
        }
        if (!is_array($data['groups'])) {
            return false;
        }
        if (!isset($data['docs'])) {
            return false;
        }
        return true;
    }

    /**
     * @param $data
     * @param $requestData
     * @param $json
     */
    protected function saveData($data, $requestData, $json)
    {
        $city = $this->saveCity($data, $requestData, $json);
        $this->saveItems($data, $city);
        $this->saveGroups($data, $city);
        $this->saveDocs($data, $city);
    }

    /**
     * @param $data
     * @param $requestData
     * @param $json
     * @return City
     * @throws LoaderException
     */
    protected function saveCity($data, $requestData, $json)
    {
        if (isset($data['city']) && is_array($data['city']) && isset($data['city']['id'])) {
            $model = City::findOne(['id' => $data['city']['id']]);

            if ($model === null) {
                $model = new City();
                $model->id = $data['city']['id'];
                $model->name = $data['city']['name'];
                $model->tkey = $data['city']['tkey'];
                $model->region_id = $data['city']['region_id'];
                $model->region = $data['city']['region'];
                $model->json = $json;
                if (empty($this->tkey)) {
                    $model->tkey = Inflector::transliterate($data['city']['name'] . '-' . $data['city']['region']);
                }
                if (!$model->save($this->validateModels)) {
                    throw new LoaderException('Can\'t save city:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
                }
            }

        } else {
            $model = City::findOne(['name' => $requestData['city'], 'region' => $requestData['area']]);
            if ($model === null) {
                $model = new City();
                $model->tkey = Inflector::transliterate($requestData['city'] . '-' . $requestData['area']);
                $model->name = $requestData['city'];
                $model->region = $requestData['area'];
                $model->json = $json;
                $id = intval(City::find()->max('id')) + 1;
                $model->id = $id;

                if (!$model->save($this->validateModels)) {
                    throw new LoaderException('Can\'t save city:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
                }
            }
        }

        return $model;

    }

    /**
     * @param $data
     * @param $city
     */
    protected function saveItems($data, $city)
    {
        foreach ($data['items'] as $item) {
            $model = $this->saveItem($item, $city);
            foreach ($item['postamats'] as $postamat) {
                $model2 = $this->savePostamat($postamat, $model, $city);
                $this->savePaymentItems($postamat['payment']['payment_items'], $model2);
            }
            $this->savePaymentItems($item['payment']['payment_items'], $model);
        }
    }

    /**
     * @param $item
     * @param City $city
     * @return Item
     * @throws LoaderException
     */
    protected function saveItem($item, $city)
    {
        $model = Item::findOne(['id' => $item['id']]);

        $isNewRecord = false;
        if ($model === null) {
            $model = new Item();
            $model['id'] = $item['id'];
            $model['tkey'] = $item['tkey'];
            $isNewRecord = true;
        }
        $model['brand_tkey'] = $item['brand_tkey'];
        $model['title'] = $item['title'];
        $model['type'] = $item['type'];
        $model['hint'] = $item['hint'];
        $model['site_name'] = $item['site_name'];
        $model['is_point'] = $item['is_point'];
        $model['comment'] = $item['comment'];
        $model['description'] = $item['description'];
        $model['duration_type'] = $item['duration_type'];
        $model['position'] = $item['position'];
        $model['delivery_group'] = $item['delivery_group'];
        $model['has_postamat'] = $item['has_postamat'];
        $model['doc-text'] = $item['doc']['text'];

        if (!$model->save($this->validateModels)) {
            throw new LoaderException('Can\'t save item:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
        }

        $relation = $isNewRecord ? null : CityHasItem::findOne([
            'city_id' => $city->id,
            'item_id' => $model->id,
        ]);

        if ($relation === null) {
            $relation = new CityHasItem();
            $relation->city_id = $city->id;
            $relation->item_id = $model->id;
        }

        $relation['price'] = $item['price'];
        $relation['currency_price'] = $item['currency_price'];
        $relation['days'] = $item['days'];
        $relation['days_work'] = $item['days_work'];
        $relation['is_enabled'] = $item['is_enabled'];
        $relation['fast_delivery_is_active'] = $item['fast_delivery']['is_active'];
        $relation['fast_delivery_period'] = $item['fast_delivery']['period'];
        $relation['fast_delivery_periods'] = is_array($item['fast_delivery']['periods']) ? Json::encode($item['fast_delivery']['periods']) : $item['fast_delivery']['periods'];
        $relation['info'] = $item['info'];
        $relation['delivery_info'] = $item['delivery_info'];
        $relation['deliverytype_period'] = $item['deliverytype_period'];

        if (!$relation->save($this->validateModels)) {
            throw new LoaderException('Can\'t save group relation:' . print_r($relation->errors, true) . PHP_EOL . print_r($relation->attributes, true));
        }

        return $model;
    }

    /**
     * @param $item
     * @param $itemModel
     * @return Postamat
     * @throws LoaderException
     */
    protected function savePostamat($item, $itemModel, $city)
    {
        $model = Postamat::findOne(['id' => $item['id']]);

        $isNewRecord = false;
        if ($model === null) {
            $model = new Postamat();
            $isNewRecord = true;
            $model['id'] = $item['id'];
        }

        $model['deliverytype_id'] = $item['deliverytype_id'];
        $model['deliverytype_city_id'] = $item['deliverytype_city_id'];
        $model['postamat_id'] = $item['postamat_id'];
        $model['zip_code'] = $item['zip_code'];
        $model['name'] = $item['name'];
        $model['description'] = $item['description'];
        $model['work_time'] = $item['work_time'];
        $model['address'] = $item['address'];
        $model['phone'] = $item['phone'];
        $model['latitude'] = $item['latitude'];
        $model['longitude'] = $item['longitude'];
        $model['is_active'] = $item['is_active'];
        $model['updated_at'] = $item['updated_at'];
        $model['type'] = $item['type'];
        $model['store_id'] = $item['store_id'];
        $model['brand_tkey'] = $item['brand_tkey'];
        $model['brand_name'] = $item['brand_name'];
        $model['deliveryTkey'] = $item['deliveryTkey'];
        $model['deliverytype_period'] = $item['deliverytype_period'];
        $model['days'] = $item['days'];
        $model['price'] = $item['price'];
        $model['currency_price'] = $item['currency_price'];

        if (!$model->save($this->validateModels)) {
            throw new LoaderException('Can\'t save postamat:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
        }

        $relation = $isNewRecord ? null : ItemHasPostamat::findOne([
            'item_id' => $itemModel->id,
            'postamat_id' => $model->id,
            'city_id' => $city->id,
        ]);

        if ($relation === null) {
            $relation = new ItemHasPostamat();
            $relation->item_id = $itemModel->id;
            $relation->postamat_id = $model->id;
            $relation->city_id = $city->id;
            if (!$relation->save($this->validateModels)) {
                throw new LoaderException('Can\'t save postamat relation:' . print_r($relation->errors, true) . PHP_EOL . print_r($relation->attributes, true));
            }
        }

        return $model;
    }

    /**
     * @param $data
     * @param Item|Postamat $itemModel
     */
    protected function savePaymentItems($data, $itemModel)
    {
        foreach ($data as $item) {
            $this->savePaymentItem($item, $itemModel);
        }
    }

    /**
     * @param $item
     * @param $itemModel
     * @return PaymentItem
     * @throws LoaderException
     */
    protected function savePaymentItem($item, $itemModel)
    {
        $model = PaymentItem::findOne(['tkey' => $item['tkey']]);

        $isNewRecord = false;
        if ($model === null) {
            $model = new PaymentItem();
            $model['tkey'] = $item['tkey'];
            $model['title'] = $item['title'];
            $isNewRecord = true;
        }

        $model['hint'] = $item['hint'];
        $model['description'] = $item['description'];
        $model['icon'] = $item['icon'];
        $model['icon_alt'] = $item['icon_alt'];
        $model['discount'] = $item['discount'];
        $model['discount_currency'] = $item['discount_currency'];
        $model['payment_tax'] = $item['payment_tax'];
        $model['percent'] = isset($item['percent']) ? $item['percent'] : '';

        if (!$model->save($this->validateModels)) {
            throw new LoaderException('Can\'t save payment item:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
        }

        if ($itemModel instanceof Item) {
            $relation = $isNewRecord ? null : PaymentItemHasItem::findOne([
                'item_id' => $itemModel->id,
                'payment_item_tkey' => $model->tkey,
            ]);
            if ($relation === null) {
                $relation = new PaymentItemHasItem();
                $relation->item_id = $itemModel->id;
                $relation->payment_item_tkey = $model->tkey;
            }
            $relation->tooltip = $item['tooltip'];
            $relation->price = $item['price'];
            $relation->currency_price = $item['currency_price'];
            if (!$relation->save($this->validateModels)) {
                throw new LoaderException('Can\'t save payment item relation:' . print_r($relation->errors, true) . PHP_EOL . print_r($relation->attributes, true));
            }
        }
        if ($itemModel instanceof Postamat) {
            $relation = $isNewRecord ? null : PaymentItemHasPostamat::findOne([
                'postamat_id' => $itemModel->id,
                'payment_item_tkey' => $model->tkey,
            ]);
            if ($relation === null) {
                $relation = new PaymentItemHasPostamat();
                $relation->postamat_id = $itemModel->id;
                $relation->payment_item_tkey = $model->tkey;
            }
            $relation->tooltip = $item['tooltip'];
            $relation->price = $item['price'];
            $relation->currency_price = $item['currency_price'];
            if (!$relation->save($this->validateModels)) {
                throw new LoaderException('Can\'t save payment item relation:' . print_r($relation->errors, true) . PHP_EOL . print_r($relation->attributes, true));
            }
        }

        return $model;
    }

    /**
     * @param $data
     * @param $city
     */
    protected function saveGroups($data, $city)
    {
        foreach ($data['groups'] as $item) {
            $this->saveGroup($item, $city);
        }
    }

    /**
     * @param $item
     * @param $city
     * @return Group
     * @throws LoaderException
     */
    protected function saveGroup($item, $city)
    {
        $model = Group::findOne(['id' => $item['id']]);

        $isNewRecord = false;
        if ($model === null) {
            $model = new Group();
            $isNewRecord = true;
            $model['id'] = $item['id'];
            $model['name'] = $item['name'];
            $model['pos'] = $item['pos'];
            $model['tkey'] = $item['tkey'];

            if (!$model->save($this->validateModels)) {
                throw new LoaderException('Can\'t save group:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
            }
        }

        $relation = $isNewRecord ? null : CityHasGroup::findOne([
            'city_id' => $city->id,
            'group_id' => $model->id,
        ]);

        if ($relation === null) {
            $relation = new CityHasGroup();
            $relation->city_id = $city->id;
            $relation->group_id = $model->id;
            if (!$relation->save($this->validateModels)) {
                throw new LoaderException('Can\'t save group relation:' . print_r($relation->errors, true) . PHP_EOL . print_r($relation->attributes, true));
            }
        }

        return $model;
    }

    /**
     * @param $data
     * @param $city
     */
    protected function saveDocs($data, $city)
    {
        foreach ($data['docs'] as $item) {
            $this->saveDoc($item, $city);
        }
    }

    /**
     * @param $item
     * @param $city
     * @return Doc
     * @throws LoaderException
     */
    protected function saveDoc($item, $city)
    {
        $model = Doc::findOne(['id' => $item['id']]);

        $isNewRecord = false;
        if ($model === null) {
            $model = new Doc();
            $isNewRecord = true;
            $model['id'] = $item['id'];
            $model['tkey'] = $item['tkey'];
        }

        $model['parent'] = $item['parent'];
        $model['title'] = $item['title'];
        $model['text'] = $item['text'];
        $model['pos'] = $item['pos'];
        $model['active'] = $item['active'];
        $model['view_str'] = $item['view_str'];
        $model['ptitle'] = $item['ptitle'];
        $model['pdescription'] = $item['pdescription'];
        $model['pkeywords'] = $item['pkeywords'];

        if (!$model->save($this->validateModels)) {
            throw new LoaderException('Can\'t save doc:' . print_r($model->errors, true) . PHP_EOL . print_r($model->attributes, true));
        }

        $relation = $isNewRecord ? null : CityHasDoc::findOne([
            'city_id' => $city->id,
            'doc_id' => $model->id,
        ]);

        if ($relation === null) {
            $relation = new CityHasDoc();
            $relation->city_id = $city->id;
            $relation->doc_id = $model->id;
            if (!$relation->save($this->validateModels)) {
                throw new LoaderException('Can\'t save doc relation:' . print_r($relation->errors, true) . PHP_EOL . print_r($relation->attributes, true));
            }
        }

        return $model;
    }

    /**
     * @param $string
     */
    protected function addError($string)
    {
        file_put_contents(Yii::getAlias($this->errorFilePath), $string . PHP_EOL, FILE_APPEND);
    }

    /**
     * @return array
     */
    public function getList()
    {
        $data = file_get_contents(Yii::getAlias($this->filePath));

        $items = explode("\n", $data);

        return array_map(function ($item) {
            $items = explode(',', $item);

            return [
                'city' => trim($items[0]),
                'area' => trim(isset($items[1]) ? $items[1] : $items[0]),
            ];

        }, $items);
    }

    /**
     * @param Response $response
     * @param $item
     * @param $index
     */
    public function processResponse($response, $item, $index)
    {
        $this->log('#' . ($index + 1) . ' ' . $item['city']);
        //$this->log('Start loading ' . $item['city']);
        $body = $response->getBody();
        $data = Json::decode($body);
        if ($this->validateData($data)) {
            try {
                $this->saveData($data, $item, $body);
            } catch (LoaderException $e) {
                $this->log('Loader exception:\n ' . $e->getMessage());
                $this->addError($item['city'] . ' ' . $e->getMessage() . $e->getTraceAsString());
            } catch (\Exception $e) {
                $this->log('Exception:\n ' . $e->getMessage());
                $this->addError($item['city'] . ' ' . $e->getMessage() . $e->getTraceAsString());
            }
        } else {
            $this->log('Not valid response at ' . $item['city']);
            $this->addError($item['city'] . ' - not valid response');
        }
        //$this->log('Finish loading ' . $item['city']);
    }

    /**
     * @param $item
     * @return array
     */
    protected function createRequestOptions($item)
    {
        return [
            'headers' => $this->headers,
            'form_params' => [
                'cart' => 'undefined',
                'client_data' => '{"address":{"address":"","area":"' . $item['area'] . '","area_type":"","buiding":"","building_housing":"","city":"' . $item['city'] . '","country":"Россия","flat":"","index":0,"street":""},"comment":"","email":"","is_subscribe_accepted":true,"lastname":"","name":"","phone":"","phone_country":"","surname":""}',
                'is_merge_delivery_points' => 'true',
                'promocode' => 'false',
            ],
        ];
    }

}