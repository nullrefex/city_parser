<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\City */
/* @var $itemDataProvider \yii\data\ActiveDataProvider */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('List', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->uid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->uid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $itemDataProvider,
        'columns' => [
            'tkey',
            'title',
            'days',
            [
                'label' => 'Price',
                'format' => 'raw',
                'value' => function ($item) {
                    $html = '';
                    $id = $item['tkey'];
                    if (isset($item['postamats']) && count($item['postamats']) > 1) {
                        $html = ['<br><button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#' . $id . '">Постаматы</button>',
                            '<table id="' . $id . '" style="margin: 0 -9px -9px;" class="table table-bordered table-striped collapse">'];
                        /** @var \app\models\ItemHasPostamat $itemHasPostamat */
                        foreach ($item['postamats'] as $itemHasPostamat) {
                            $html[] = '<tr>' .
                                '<td>' .
                                $itemHasPostamat->postamat->address .
                                '</td><td>' .
                                $itemHasPostamat->postamat->price . ' руб.' .
                                '</td>' .
                                '</tr>';
                        }
                        $html[] = '</table>';
                        $html = implode('', $html);
                    }
                    return $item['price'] . $html;
                }
            ],
        ],
    ]) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'uid',
            'id',
            'name',
            'tkey',
            'region_id',
            'region',
            'json:ntext',
        ],
    ]) ?>


</div>
