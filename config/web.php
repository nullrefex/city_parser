<?php

return [
    'id' => 'city-parser-web',
    'language' => 'ru',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'runtimePath' => dirname(__DIR__) . '/runtime',
    'bootstrap' => [
        'log',
    ],
    'defaultRoute' => 'city',
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => [],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
        ],
    ],
];
