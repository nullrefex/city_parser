<?php

$config = [
    'id' => 'city-parser',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\commands',
    'bootstrap' => ['gii'],
    'components' => [
        'db' => require 'db.php',
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
        ],
    ],

];

return $config;